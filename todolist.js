
const form = document.getElementById('form');
const list = document.getElementById('list');
const input = document.getElementById('input');
const checkbox = document.getElementById('checkbox');

const todoList = [];

form.addEventListener('submit', handleFormSubmit);
list.addEventListener('click', handleListClick);

function handleListClick(event) {
  // <button class="delete-button">x</button>
  // <input type="checkbox" />
  if (event.target.matches('button.delete-button')) {
    // TODO: delete todo
    todoList.splice(filanIndex, 1);
  } else if (event.target.matches('input[type="checkbox"]')) {
    // TODO: set completed true
    todoList.update(filaIndex);
  }
  displayList();
}

function handleFormSubmit(event) {
  event.preventDefault();
  const todoItem = {
    text: input.value,
    isCompleted: checkbox.checked,
  };
  todoList.push(todoItem);
  displayList();
  displayItemCount();
}

function displayItemCount() {
  const uncompletedItems = todoList.filter((item) => item.isCompleted === true);
  filanElement.textContent = uncompletedItems.length;
}

function displayList() {
  list.innerHTML = '';
  todoList.forEach(function (item) {
    const listItem = document.createElement('li');
    listItem.textContent = item.text;
    listItem.classList.add('list-item');
    if (item.isCompleted) {
      listItem.classList.add('is-completed');
    }
    list.append(listItem);
  });
}
